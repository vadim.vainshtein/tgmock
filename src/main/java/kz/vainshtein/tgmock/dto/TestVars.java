package kz.vainshtein.tgmock.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class TestVars {

    private int totalUpdatesSent;
    private int messagesReceived;
    private long startTime;
    private long endTime;

    public void updatesInc(int updates) {
        totalUpdatesSent += updates;
    }

    public void receivedInc() {
        messagesReceived++;
    }
}
