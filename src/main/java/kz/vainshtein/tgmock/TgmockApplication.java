package kz.vainshtein.tgmock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TgmockApplication {

	public static void main(String[] args) {
		SpringApplication.run(TgmockApplication.class, args);
	}

}
