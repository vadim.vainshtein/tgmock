package kz.vainshtein.tgmock;

import org.telegram.telegrambots.meta.api.methods.updates.GetUpdates;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ScheduledUpdates {

    private List<Update> updates;
    private int firstUpdateId;

    public ScheduledUpdates(int firstUpdateId, long chatId, int contestMessageId, int lastMessageId, int size) {

        updates = new ArrayList<>(size);
        this.firstUpdateId = firstUpdateId;

        var user = createUser();
        var bot = createBot();
        var chat = createChat(-1001968141579L, "supergroup");

        var replyToMessage = new Message();
        var forwardFromChat = createChat(chatId, "channel");

        replyToMessage.setFrom(bot);
        replyToMessage.setMessageId(contestMessageId);
        replyToMessage.setMessageThreadId(contestMessageId);
        replyToMessage.setChat(chat);
        replyToMessage.setDate((int) System.currentTimeMillis());
        replyToMessage.setForwardFromChat(forwardFromChat);
        replyToMessage.setForwardFromMessageId(contestMessageId);

        char charIndex = 'A';
        int intIndex = 1;

        for (var i = 0; i < size; i++) {
            var update = new Update();
            var message = new Message();

            message.setChat(chat);
            message.setFrom(user);
            message.setSenderChat(chat);
            message.setDate((int) System.currentTimeMillis());
            message.setText(Character.toString(charIndex) + intIndex++);
            if (intIndex > 50) {
                intIndex = 1;
                if (charIndex++ >= 'Z') charIndex = 'A';
            }
            message.setMessageThreadId(contestMessageId);
            message.setMessageId(lastMessageId + i + 1);

            message.setReplyToMessage(replyToMessage);

            update.setUpdateId(firstUpdateId + i);
            update.setMessage(message);

            updates.add(update);
        }
    }

    private Chat createChat(Long chatId, String chatType) {
        var chat = new Chat();

        chat.setId(chatId);
        chat.setTitle("Sample chat");
        chat.setType(chatType);
        return chat;
    }

    private static User createUser() {
        var user = new User();

        user.setUserName("sample_user");
        user.setId(1234567890L);
        user.setFirstName("Sample");
        return user;
    }

    private static User createBot() {
        var bot = new User();

        bot.setId(1234543210L);
        bot.setFirstName("sample_bot");
        bot.setUserName("bot_bot");
        bot.setIsBot(true);

        return bot;
    }

    public List<Update> get(GetUpdates getUpdates) {

        var limit = Math.min(100, getUpdates.getLimit());
        var form = getUpdates.getOffset() - firstUpdateId;
        var to = form + limit;
        var size = updates.size();
        if (to > size) to = size;

        if (form < 0 || to <= form) {
            return Collections.emptyList();
        } else {
            return updates.subList(form, to);
        }
    }
}
