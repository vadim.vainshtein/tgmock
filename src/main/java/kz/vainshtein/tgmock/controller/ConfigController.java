package kz.vainshtein.tgmock.controller;

import kz.vainshtein.tgmock.AppConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static java.util.Objects.nonNull;

@RestController
@RequestMapping("/conf")
@RequiredArgsConstructor
public class ConfigController {

    private final AppConfig appConfig;

    @GetMapping
    public AppConfig config(
            @RequestParam(name = "initMessageId", required = false) Integer messageId,
            @RequestParam(name = "channelChatId", required = false) Long chatId,
            @RequestParam(name = "updatesToSend", required = false) Integer updatesToSend) {

        if (nonNull(messageId)) appConfig.setInitMessageId(messageId);
        if (nonNull(chatId)) appConfig.setChannelChatId(chatId);
        if (nonNull(updatesToSend)) appConfig.setUpdatesToSend(updatesToSend);

        return appConfig;
    }
}
