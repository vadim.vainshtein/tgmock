package kz.vainshtein.tgmock.controller;

import kz.vainshtein.tgmock.AppConfig;
import kz.vainshtein.tgmock.ScheduledUpdates;
import kz.vainshtein.tgmock.dto.MyApiResponse;
import kz.vainshtein.tgmock.dto.TestVars;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updates.GetUpdates;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/{botToken}")
@Slf4j
@RequiredArgsConstructor
public class BotController {

    private final AppConfig appConfig;
    private final TestVars testVars;

    private ScheduledUpdates scheduledUpdates;

    private int lastMessageId;

    private int nextUpdateId = 0;


    @PostMapping("/getupdates")
    public MyApiResponse<List<Update>> getUpdates(@RequestBody GetUpdates updatesRequest) {

        nextUpdateId = updatesRequest.getOffset();

        var response = new MyApiResponse<List<Update>>();
        response.setOk(true);

        if (scheduledUpdates == null || testVars.getTotalUpdatesSent() >= appConfig.getUpdatesToSend()) {
            response.setResult(Collections.emptyList());
            return response;
        }

        var updates = scheduledUpdates.get(updatesRequest);
        response.setResult(updates);
        testVars.setEndTime(System.currentTimeMillis());

        var updatesSize = updates.size();
        lastMessageId += updatesSize;
        testVars.updatesInc(updatesSize);
        log.info("Get updates: OFFSET: {}, SENT: {}, TOTAL: {}", updatesRequest.getOffset(), updatesSize, testVars.getTotalUpdatesSent());
        return response;
    }

    @PostMapping("deleteWebhook")
    public MyApiResponse<Boolean> deleteWebhook() {
        var response = new MyApiResponse<Boolean>();
        response.setOk(true);
        response.setResult(true);
        response.setErrorDescription("Webhook is already deleted");

        return response;
    }

    @PostMapping("sendmessage")
    public MyApiResponse<Message> sendMessage(@RequestBody SendMessage sendMessage) {

        var text = sendMessage.getText();
        var chatId = sendMessage.getChatId();

        var message = new Message();
        var chat = createChat(Long.parseLong(chatId), "supergroup");

        testVars.receivedInc();
        testVars.setEndTime(System.currentTimeMillis());

        message.setMessageId(lastMessageId++);
        message.setSenderChat(chat);
        message.setChat(chat);
        message.setDate((int) System.currentTimeMillis());
        message.setText(text);

        var response = new MyApiResponse<Message>();
        response.setOk(true);
        response.setResult(message);

        return response;
    }

    @GetMapping("/generate")
    public void generateUpdates() {
        scheduledUpdates = new ScheduledUpdates(nextUpdateId, appConfig.getChannelChatId(), 0, lastMessageId, appConfig.getUpdatesToSend());
    }

    private Chat createChat(Long chatId, String chatType) {
        var chat = new Chat();

        chat.setId(chatId);
        chat.setTitle("Sample chat");
        chat.setType(chatType);
        return chat;
    }
}
