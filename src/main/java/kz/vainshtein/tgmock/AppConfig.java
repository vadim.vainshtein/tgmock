package kz.vainshtein.tgmock;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class AppConfig {
    private int initMessageId = 1;
    private long channelChatId = -1001832883224L;
    private int updateInterval = 0;
    private int updatesToSend = 5000;
}
